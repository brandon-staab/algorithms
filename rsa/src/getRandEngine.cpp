/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "getRandEngine.hpp"


namespace {
	std::random_device rng{};
	std::mt19937_64 generator{rng()};
}  // namespace


std::mt19937_64&
getRandEngine() {
	return generator;
}
