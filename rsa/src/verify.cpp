/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "verify.hpp"

#include "encrypt.hpp"


BigUnsigned
verify(const BigUnsigned& sig, const BigUnsigned& e, const BigUnsigned& n) {
	return encrypt(sig, e, n);
}
