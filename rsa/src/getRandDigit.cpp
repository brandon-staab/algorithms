/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "getRandDigit.hpp"

#include "getRandEngine.hpp"

#include <limits>


int
getRandDigit() {
	static std::uniform_int_distribution<int> distribution(0, 9);

	return distribution(getRandEngine());
}
