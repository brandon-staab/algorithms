/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "fermatPrimalityTest.hpp"


bool
fermatPrimalityTest(const BigUnsigned& candidate, const std::vector<BigUnsigned>& test_list) {
	BigUnsigned exponent = candidate - 1;

	for (BigUnsigned base : test_list) {
		if (modexp(base, exponent, candidate) != 1) {
			return false;
		}
	}

	return true;
}
