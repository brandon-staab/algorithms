/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "sha256file.hpp"
#include "sign.hpp"
#include "verify.hpp"

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>


int
main(int argc, char const* argv[]) {
	if (!(argc == 3 || argc == 4)) {
		goto usage;
	} else {
		try {
			std::string input_file(argv[2]), signature_file, line;
			if (argc == 4) {  // use supplied signature file
				signature_file = argv[3];
			} else {  // use default signature file
				signature_file = input_file + ".sig";
			}

			// calculate SHA-256 of file
			BigUnsigned hash = sha256file(input_file);

			if (std::strcmp(argv[1], "s") == 0) {  // sign
				BigUnsigned d, n;

				// load d and n
				std::ifstream d_n("d_n.txt");
				if (d_n.is_open()) {
					std::getline(d_n, line);
					d = stringToBigUnsigned(line);
					std::getline(d_n, line);
					n = stringToBigUnsigned(line);
					d_n.close();
				} else {
					throw std::runtime_error("Unable to open file \"d_n.txt\"");
				}

				// sign the hash
				BigUnsigned signature = sign(hash, d, n);

				// save signature
				std::ofstream sig(signature_file.c_str(), std::ofstream::trunc);
				if (sig.is_open()) {
					sig << signature;
					sig.close();
				} else {
					throw std::runtime_error(std::string("Unable to open file \"") + argv[2]);
				}
			} else if (std::strcmp(argv[1], "v") == 0) {  // verify
				BigUnsigned e, n;

				// load e and n
				std::ifstream e_n("e_n.txt");
				if (e_n.is_open()) {
					std::getline(e_n, line);
					e = stringToBigUnsigned(line);
					std::getline(e_n, line);
					n = stringToBigUnsigned(line);
					e_n.close();
				} else {
					throw std::runtime_error("Unable to open file \"d_n.txt\"");
				}

				// load signature
				BigUnsigned signature;
				std::ifstream sig(signature_file.c_str());
				if (sig.is_open()) {
					std::getline(sig, line);
					signature = stringToBigUnsigned(line);
					sig.close();
				} else {
					throw std::runtime_error(signature_file);
				}

				// check signature
				if (hash == verify(signature, e, n)) {
					std::cout << "Signature is valid." << std::endl;
				} else {
					std::cout << "Signature is not valid!" << std::endl;
				}
			} else {
				goto usage;
			}

			return 0;
		} catch (std::exception& e) {
			std::cerr << "Exception caught: " << e.what() << std::endl;
		} catch (char const* err) {
			std::cerr << "Exception caught: " << err << std::endl;
		}

		return 1;
	}
usage:
	std::cout << "usage: sign <s|v> <input_file> [signature_file]" << std::endl;
	return 1;
}
