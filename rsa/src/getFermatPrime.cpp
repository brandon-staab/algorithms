/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "getFermatPrime.hpp"

#include "fermatPrimalityTest.hpp"
#include "getLargeRandInt.hpp"
#include "getRandDigit.hpp"

#include <algorithm>
#include <atomic>
#include <thread>
#include <utility>
#include <vector>


namespace {
	using CandidatePairs = std::vector<std::pair<std::atomic_bool, BigUnsigned>>;

	void
	getFermatPrimeHelper(const std::size_t worker_index, const std::size_t digits, CandidatePairs& candidates) {
		bool found_prime = false;
		unsigned char i = 0;

		while (!found_prime) {
			// check if another helper found a prime
			for (auto& pair : candidates) {
				if (pair.first) {
					found_prime = true;
					return;
				}
			}

		restart:
			candidates[worker_index].second = getLargeRandInt(digits);
			i = 0;

		add_digit:
			// add variance to length of primes
			if (++i == 8) {
				goto restart;
			}
			candidates[worker_index].second *= 10;
			candidates[worker_index].second += getRandDigit();

			// rule out easy composite numbers
			std::size_t ones = BigUnsignedInABase(candidates[worker_index].second, 10).getDigit(0);
			if (ones % 2 == 0 || ones == 5) {
				goto add_digit;
			}

			// check to see if it passes the test
			candidates[worker_index].first = fermatPrimalityTest(candidates[worker_index].second, {2, 7});
		}
	}
}  // namespace


BigUnsigned
getFermatPrime(const std::size_t digits) {
	std::vector<std::thread> threads;

	// spawn enough threads to saturate the cpu
	std::size_t threadsToSpawn = std::thread::hardware_concurrency();
	if (threadsToSpawn == 0) {
		threadsToSpawn = 1;
	};

	// get task ready to complete
	CandidatePairs candidates(threadsToSpawn);
	std::for_each(candidates.begin(), candidates.end(), [](std::pair<std::atomic_bool, BigUnsigned>& candidate) { candidate.first = false; });

	// give helpers their tasks
	for (std::size_t worker_index = 0; worker_index < threadsToSpawn; ++worker_index) {
		threads.emplace_back(std::thread{getFermatPrimeHelper, worker_index, digits, std::ref(candidates)});
	}

	// wait for the helpers to stop
	for (auto& th : threads) {
		th.join();
	}

	// return found fermat prime
	for (auto& pair : candidates) {
		if (pair.first) {
			return pair.second;
		}
	}

	throw;
}
