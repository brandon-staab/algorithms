/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "fermatPrimalityTest.hpp"
#include "getFermatPrime.hpp"

#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdexcept>


using namespace std::chrono;


int
main(int argc, char const* argv[]) {
	int bits = 1024;
	BigUnsigned p, q, n, e, d, totient;
	std::ofstream p_q, e_n, d_n;
	steady_clock::time_point start = steady_clock::now();

	try {
		if (!(argc == 1 || argc == 2)) {
			goto usage;
		} else {
			// calculate required the amount of digits
			if (argc == 2) {  // update the required amount of bits if supplied
				bits = std::stoi(std::string(argv[1]));
				if (bits < 128) {
					bits = 128;
				}
			}
			const std::size_t digits = ceil(bits * log10(2));
			std::cout << "bits: " << bits << ", digits: " << digits << '\n';

		restart:
			try {
				// generate primes
				p = getFermatPrime(digits);
				q = getFermatPrime(digits);

				// save p and q
				p_q.open("p_q.txt", std::ofstream::out | std::ofstream::trunc);
				if (p_q.is_open()) {
					p_q << p << '\n' << q;
					p_q.close();
				} else {
					throw std::runtime_error("Unable to open file \"p_q.txt\"");
				}

				// generate key pairs
				n = p * q;
				totient = lcm(p - 1, q - 1);
				e = 65537;               // encryption exponent
				d = modinv(e, totient);  // decryption exponent

				// save public key pair
				e_n.open("e_n.txt", std::ofstream::out | std::ofstream::trunc);
				if (e_n.is_open()) {
					e_n << e << '\n' << n;
					e_n.close();
				} else {
					throw std::runtime_error("Unable to open file \"e_n.txt\"");
				}

				// save private key pair
				d_n.open("d_n.txt", std::ofstream::out | std::ofstream::trunc);
				if (d_n.is_open()) {
					d_n << d << '\n' << n;
					d_n.close();
				} else {
					throw std::runtime_error("Unable to open file \"d_n.txt\"");
				}
			} catch (char const* err) {
				std::cerr << "Exception caught: " << err << "\nTrying again" << std::endl;
				goto restart;
			}

			std::cout << "Execution time: " << duration_cast<duration<double>>(steady_clock::now() - start).count() << 's' << std::endl;
			return 0;
		}

	} catch (const std::out_of_range& oor) {
		std::cerr << argv[1] << " is not int the domain of [128, 2147483647]\n";
	} catch (const std::exception& ex) {
		if (ex.what() == std::string("stoi")) {
			std::cerr << "\"num_of_bits\" must be an integer. Got: " << argv[1] << '\n';
		} else {
			std::cerr << ex.what() << '\n';
		}
	}

usage:
	std::cout << "usage: rsa435 [num_of_bits]" << std::endl;
	return 1;
}
