/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "getLargeRandInt.hpp"

#include "getRandDigit.hpp"


BigUnsigned
getLargeRandInt(const BigUnsigned& digits) {
	BigUnsigned x = getRandDigit();
	for (BigUnsigned i = 0; i < digits; ++i) {
		x *= 10;
		x += getRandDigit();
	}

	return x;
}
