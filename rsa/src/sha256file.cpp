/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "sha256file.hpp"

#include "sha256.hpp"

#include <fstream>
#include <iostream>


BigUnsigned
sha256file(const std::string filename) {
	std::string hash;
	std::streampos begin, end, size;
	std::ifstream file(filename.c_str(), std::ifstream::binary);

	if (file.is_open()) {
		// get size of the file in bytes
		begin = file.tellg();
		file.seekg(0, std::ios::end);
		end = file.tellg();
		size = end - begin;

		char* memblock = new char[size];

		// read the entire file into memory
		file.seekg(0, std::ios::beg);
		file.read(memblock, size);
		file.close();

		hash = sha256(memblock, memblock + size);

		delete[] memblock;
	} else {
		std::cerr << "Could not open file \"" << filename << "\"\n";
	}

	return BigUnsignedInABase(hash, 16);
}
