High-level Explanation of Implementation
----------------------------------------
To generate the public and private key pairs "rsa435" spawns a couple
helper threads that checks and see if the chosen number is a Fermat
prime. After two suitable primes are found "rsa435" generates the keys
as documented at: https://en.wikipedia.org/wiki/RSA_(cryptosystem)

To sign a file "sign" signs a SHA-256 hash of the input_file using the
private key.  "sign" verifies a signature by comparing the SHA-256
hash of the input_file and the result of encrypting the signature_file
with the public key.


Notes
-----
How to Build:

git clone git@gitlab.com:brandon-staab/algorithms.git &&
mkdir algorithms/rsa/build &&
cd algorithms/rsa/build/ &&
cmake .. &&
make


How to Use:

usage: rsa435 [num_of_bits]
Will generate "q_q.txt" and coresponding public key ("e_n.txt") and
private key ("d_n.txt") pairs. P and Q will both be at least the
specified amount of bits (default is 1024).

usage: sign <s|v> <input_file> [signature_file]
s				Sign mode
v				Verify mode
input_file		File to be signed or checked
signature_file	File to store signature to or load signature from.
				If no file is supplied it will default to <input_file>.sig
