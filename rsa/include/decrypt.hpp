/**
 *  \brief		Project 1: RSA & Digital Signature
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "BigIntegerLibrary.hh"


BigUnsigned decrypt(const BigUnsigned& cipher, const BigUnsigned& d, const BigUnsigned& n);
