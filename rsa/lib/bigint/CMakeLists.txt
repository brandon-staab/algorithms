add_library(
	bigint
	${CMAKE_CURRENT_SOURCE_DIR}/src/BigInteger.cc
	${CMAKE_CURRENT_SOURCE_DIR}/src/BigIntegerAlgorithms.cc
	${CMAKE_CURRENT_SOURCE_DIR}/src/BigIntegerUtils.cc
	${CMAKE_CURRENT_SOURCE_DIR}/src/BigUnsigned.cc
	${CMAKE_CURRENT_SOURCE_DIR}/src/BigUnsignedInABase.cc
)

target_include_directories(
	bigint PUBLIC
	${CMAKE_CURRENT_SOURCE_DIR}/include
)
