/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "load.hpp"
#include "save.hpp"
#include "seam_carve.hpp"

#include <iostream>
#include <stdexcept>


int
main(int argc, char const* argv[]) {
	PGM pgm;
	std::string filename;
	std::size_t horizontal, vertical;

	if (argc != 4) {
		goto usage;
	}

	try {
		filename = std::string(argv[1]);
		vertical = std::stoi(argv[2]);
		horizontal = std::stoi(argv[3]);

		pgm = load(filename);

		if (pgm.height <= vertical) {
			throw std::invalid_argument("Can not remove more vertical pixels than exist");
		} else if (pgm.width <= horizontal) {
			throw std::invalid_argument("Can not remove more horizontal pixels than exist");
		}

		seam_carve(pgm, horizontal, vertical);
		save(filename.substr(0, filename.length() - 4) + "_processed.pgm", pgm);
		std::cout << std::endl;
		return 0;
	} catch (std::exception& e) {
		std::cerr << "Exception Caught: " << e.what() << '\n';
		goto usage;
	}

usage:
	std::cout << "usage: seam-carver <filename> <vertical> <horizontal>" << std::endl;
	return 1;
}
