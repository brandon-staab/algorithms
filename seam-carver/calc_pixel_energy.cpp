/**
 *  \brief		Proiect 3: Seam Carving
 *  \details	Algorithms proiect for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "calc_pixel_energy.hpp"

#include <cstdlib>
#include <iostream>


void
calc_pixel_energy(const PGM& pgm, PGM::GrayMap& energy, const std::size_t i, const std::size_t j) {
	int top = 0, bottom = 0, left = 0, right = 0;

	// top
	if (j != 0) {
		top = pgm.gray_map[j][i] - pgm.gray_map[j - 1][i];
	}

	// bottom
	if (j != pgm.height - 1) {
		bottom = pgm.gray_map[j][i] - pgm.gray_map[j + 1][i];
	}

	// left
	if (i != 0) {
		left = pgm.gray_map[j][i] - pgm.gray_map[j][i - 1];
	}

	// right
	if (i != pgm.width - 1) {
		right = pgm.gray_map[j][i] - pgm.gray_map[j][i + 1];
	}

	// j is height, i is width
	energy[j][i] = std::abs(top) + std::abs(bottom) + std::abs(left) + std::abs(right);
}
