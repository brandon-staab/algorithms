/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <string>
#include <vector>


struct PGM {
	using gray_size_t = unsigned short int;
	using GrayMap = std::vector<std::vector<PGM::gray_size_t>>;

	std::string version;
	std::size_t width, height, depth;
	GrayMap gray_map;
};
