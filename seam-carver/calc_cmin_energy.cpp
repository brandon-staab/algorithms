/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "calc_cmin_energy.hpp"

#include "calc_pixel_cmin_energy.hpp"


void
calc_cmin_energy(const PGM& pgm, const PGM::GrayMap& energy, PGM::GrayMap& cmin_energy, const bool is_vertical) {
	if (is_vertical) {
		for (std::size_t j = 0; j < pgm.height; ++j) {
			for (std::size_t i = 0; i < pgm.width; ++i) {
				calc_pixel_cmin_energy(energy, cmin_energy, i, j, is_vertical);
			}
		}
	} else {
		for (std::size_t i = 0; i < pgm.width; ++i) {
			for (std::size_t j = 0; j < pgm.height; ++j) {
				calc_pixel_cmin_energy(energy, cmin_energy, i, j, is_vertical);
			}
		}
	}
}
