/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "save.hpp"

#include <fstream>
#include <stdexcept>


void
save(const std::string filename, const PGM& pgm) {
	std::ofstream file(filename.c_str());

	if (file.is_open()) {
		// save meta data
		file << pgm.version << '\n' << pgm.width << ' ' << pgm.height << '\n' << pgm.depth << '\n';

		// save grayscale values
		for (auto y : pgm.gray_map) {
			for (auto x : y) {
				file << x << ' ';
			}
			file << '\n';
		}
	} else {
		throw std::runtime_error("Unable to open " + filename);
	}
}
