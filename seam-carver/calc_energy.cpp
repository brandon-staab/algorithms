/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "calc_energy.hpp"

#include "calc_pixel_energy.hpp"


void
calc_energy(const PGM& pgm, PGM::GrayMap& energy) {
	for (std::size_t j = 0; j < pgm.height; ++j) {
		for (std::size_t i = 0; i < pgm.width; ++i) {
			calc_pixel_energy(pgm, energy, i, j);
		}
	}
}
