/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "calc_pixel_cmin_energy.hpp"

#include <algorithm>


void
calc_pixel_cmin_energy(const PGM::GrayMap& energy, PGM::GrayMap& cmin_energy, const std::size_t i, const std::size_t j, const bool is_vertical) {
	PGM::gray_size_t min = 0;  // base case is zero

	if (is_vertical) {
		if (j != 0) {  // top edge base case when equal to zero
			// up
			min = cmin_energy[j - 1][i];

			if (i != 0) {  // up to the left
				min = std::min(min, cmin_energy[j - 1][i - 1]);
			}

			if (i != energy[0].size() - 1) {  // up to the right
				min = std::min(min, cmin_energy[j - 1][i + 1]);
			}
		}
	} else {
		if (i != 0) {  // left edge base case when equal to zero
			cmin_energy[j][i] = cmin_energy[j][i];
			// left
			min = cmin_energy[j][i - 1];

			if (j != 0) {  // left to the top
				min = std::min(min, cmin_energy[j - 1][i - 1]);
			}

			if (j != energy.size() - 1) {  // left to the bottom
				min = std::min(min, cmin_energy[j + 1][i - 1]);
			}
		}
	}

	cmin_energy[j][i] = energy[j][i] + min;
}
