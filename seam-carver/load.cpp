/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "load.hpp"

#include <fstream>
#include <stdexcept>


PGM
load(const std::string filename) {
	PGM pgm;
	std::string line;
	std::ifstream file(filename.c_str());

	if (file.is_open()) {
		// load meta data
		std::size_t counter = 0;
		while (std::getline(file, line)) {
			// discard comments
			if (line[0] != '#') {
				if (counter == 0) {
					// only support version P2
					if (0 == line.compare("P2\n")) {
						throw std::domain_error("Bad pgm: version " + line);
					}

					pgm.version = line;
				} else if (counter == 1) {
					std::size_t idx;
					pgm.width = std::stoi(line, &idx);
					line = line.substr(idx);
					pgm.height = std::stoi(line);
				} else if (counter == 2) {
					pgm.depth = std::stoi(line);
				}

				// break loop after meta is read
				if (++counter > 2) {
					break;
				}
			}
		}

		// reserve enough space
		pgm.gray_map = PGM::GrayMap(pgm.height, std::vector<PGM::gray_size_t>(pgm.width, 0));

		// load grayscale values
		std::size_t x = 0, y = 0, pos, len;
		while (std::getline(file, line)) {
			pos = 0;

			// discard comments
			if (line[0] != '#') {
				while (pos < line.length() - 1) {
					if (y < pgm.height) {
						if (x < pgm.width) {
							// get value
							auto temp = std::stoul(line.substr(pos), &len);
							if (0 <= temp && temp <= 65536) {
								pgm.gray_map[y][x] = static_cast<PGM::gray_size_t>(temp);
							} else {
								throw std::domain_error("Bad pgm: grayscale value");
							}

							// update offsets
							pos += len + 1;
							x++;
						} else {
							x = 0;
							y++;
						}
					} else {
						throw std::domain_error("Bad pgm: dimensions");
					}
				}
			}
		}
		file.close();
	} else {
		throw std::runtime_error("Unable to open " + filename);
	}

	return pgm;
}
