/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */


#include "seam_carve.hpp"
#include "calc_cmin_energy.hpp"
#include "calc_energy.hpp"
#include "carve.hpp"

#include <iostream>


void seam_carve(PGM& pgm, std::size_t horizontal, std::size_t vertical) {
	PGM::GrayMap energy, cmin_energy;

	// remove vertical
	while (vertical--) {
		energy = cmin_energy = PGM::GrayMap(pgm.height, std::vector<PGM::gray_size_t>(pgm.width, 0));
		calc_energy(pgm, energy);
		calc_cmin_energy(pgm, energy, cmin_energy);
		carve(pgm, cmin_energy);
	}

	// remove horizontal
	while (horizontal--) {
		energy = cmin_energy = PGM::GrayMap(pgm.height, std::vector<PGM::gray_size_t>(pgm.width, 0));
		calc_energy(pgm, energy);
		calc_cmin_energy(pgm, energy, cmin_energy, false);
		carve(pgm, cmin_energy, false);
	}
}
