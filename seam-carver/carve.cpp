/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "carve.hpp"

#include <algorithm>
#include <iostream>


void
carve(PGM& pgm, const PGM::GrayMap& cmin_energy, const bool is_vertical) {
	PGM::gray_size_t min, temp;
	if (is_vertical) {
		std::size_t j = pgm.height;
		std::vector<std::size_t> vertical_cut(j--, 0);

		// find min endpoint
		temp = min = cmin_energy[j][pgm.width - 1];
		for (std::size_t i = pgm.width - 2; i + 1 != 0; --i) {
			min = std::min(cmin_energy[j][i], min);
			if (temp != min) {
				temp = min;
				vertical_cut[j] = i;
			}
		}

		// trace back
		while (j > 0) {
			std::size_t i = vertical_cut[j--];

			// up to the right
			if (i != pgm.width - 1) {
				min = cmin_energy[j][i + 1];
				vertical_cut[j] = i + 1;
			}

			// up
			if (i == pgm.width - 1 || (cmin_energy[j][i] <= min && i != pgm.width - 1)) {
				min = cmin_energy[j][i];
				vertical_cut[j] = i;
			}

			// up to the left
			if (i != 0 && cmin_energy[j][i - 1] <= min) {
				min = cmin_energy[j][i - 1];
				vertical_cut[j] = i - 1;
			}
		}

		// carve out seam
		for (size_t height = 0; height < pgm.height; ++height) {
			for (size_t width = vertical_cut[height]; width < pgm.width - 1; ++width) {
				pgm.gray_map[height][width] = pgm.gray_map[height][width + 1];
			}
		}

		for (auto& row : pgm.gray_map) {
			row.pop_back();
		}
		pgm.width--;
	} else {
		std::size_t i = pgm.width;
		std::vector<std::size_t> horizontal_cut(i--, 0);

		// find min endpoint
		temp = min = cmin_energy[0][i];
		for (std::size_t j = 1; j < pgm.height; ++j) {
			min = std::min(cmin_energy[j][i], min);
			if (temp != min) {
				temp = min;
				horizontal_cut[i] = j;
			}
		}

		// trace back
		while (i > 0) {
			std::size_t j = horizontal_cut[i--];

			// left
			temp = min = cmin_energy[j][i];
			horizontal_cut[i] = j;

			// left and up
			if (j != 0) {
				min = std::min(min, cmin_energy[j - 1][i]);
				if (temp != min) {
					temp = min;
					horizontal_cut[i] = j - 1;
				}
			}

			// left and down
			if (j != pgm.height - 1) {
				min = std::min(min, cmin_energy[j + 1][i]);
				if (temp != min) {
					temp = min;
					horizontal_cut[i] = j + 1;
				}
			}
		}

		// carve out seam
		for (size_t width = 0; width < pgm.width; ++width) {
			for (size_t height = horizontal_cut[width]; height < pgm.height - 1; ++height) {
				pgm.gray_map[height][width] = pgm.gray_map[height + 1][width];
			}
		}

		pgm.gray_map.pop_back();
		pgm.height--;
	}
}
