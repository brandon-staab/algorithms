/**
 *  \brief		Project 3: Seam Carving
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		April 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "PGM.hpp"


void seam_carve(PGM& pgm, std::size_t horizontal, std::size_t vertical);
