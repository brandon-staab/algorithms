High-level Explanation of Implementation
-------------------------------------------------
This seam-carver uses dynamic programming to remove
vertical and horizontal seams from a PGM file.  It
does this by calculating an energy matrix (the
gradient magnitude).  After that the energy matrix
is used to calculate the cumulative minimum energy
for all possible seams.  Finally, the seam is
removed from the PGM and all the remaining values
are shifted together.


Notes
-------------------------------------------------
How to Build:

git clone git@gitlab.com:brandon-staab/algorithms.git &&
mkdir algorithms/seam-carver/build &&
cd algorithms/seam-carver/ &&
make


How to Use:

usage: usage: seam-carver <filename> <vertical> <horizontal>
	vertical	amount of vertical pixels to remove
	horizontal	amount of horizontal pixels to remove
