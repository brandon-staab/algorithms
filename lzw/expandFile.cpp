/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "expandFile.hpp"

#include "decompress.hpp"
#include "open.hpp"

#include <fstream>


void
expandFile(const std::string input_filename, const std::string output_filename, const std::size_t code_base_len, const std::size_t code_max_len, const bool reset_full_dictionary) {
	auto compressed = open(input_filename);

	std::ofstream file(output_filename.c_str());
	if (!file.is_open()) {
		throw "File was not opened";
	}

	file << decompress(compressed, code_base_len, code_max_len, reset_full_dictionary);

	file.close();
}
