/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "compressFile.hpp"

#include "compress.hpp"

#include <cmath>
#include <fstream>
#include <sstream>


void
compressFile(const std::string input_filename,
			 const std::string output_filename,
			 const std::size_t code_base_len,
			 const std::size_t code_max_len,
			 const bool reset_full_dictionary) {
	// Load input_file into buffer
	std::stringstream buffer;
	std::ifstream input_file(input_filename.c_str());
	if (!input_file.is_open()) {
		throw "File was not opened";
	}
	buffer << input_file.rdbuf();
	input_file.close();

	// Open output_file
	std::ofstream output_file(output_filename.c_str(), std::ios::binary);
	if (!output_file.is_open()) {
		throw "File was not opened";
	}

	compress(buffer, output_file, code_base_len, std::pow(2, code_max_len), reset_full_dictionary);
	output_file.close();
}
