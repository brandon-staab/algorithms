/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "binaryString2Int.hpp"
#include "int2BinaryString.hpp"

#include <cmath>
#include <iostream>
#include <map>


namespace {
	const std::string zeros = "00000000";
}


void
compress(std::iostream& input, std::ostream& output, std::size_t word_size, const std::size_t abs_max_entries, const bool reset_full_dictionary) {
	std::size_t max_dictionary_size = std::pow(2, word_size);
	std::string word, word_c, bin_str;

	// Build dictionary with ASCII table
	std::map<std::string, int> dictionary;
	for (int i = 0; i < 256; ++i) {
		dictionary[std::string(1, i)] = i;
	}

	// Finish compressing
	while (input.good()) {
		char c = input.get();
		word_c = word + c;

		if (dictionary.count(word_c)) {
			word = word_c;
		} else {  // word_c not found in dictionary
			// Append word to bin_str
			bin_str += int2BinaryString(dictionary[word], word_size);

			// Add word_c to the dictionary
			if (dictionary.size() < abs_max_entries) {
				auto gpp6_bug_fix = dictionary.size();
				dictionary[word_c] = gpp6_bug_fix;

				// Check if we need more bits to encode with
				if (dictionary.size() > max_dictionary_size) {
					word_size++;
					max_dictionary_size = std::pow(2, word_size);
				}
			} else if (reset_full_dictionary) {  // Dictionary full and no more bits left to encode with
				throw "Part 3";
			}

			word = std::string(1, c);
		}

		// Write to the file as bin_str gets large
		if (bin_str.length() >= 4096) {
			do {
				output << static_cast<char>(binaryString2Int(bin_str.substr(0, 8)));
				bin_str = bin_str.substr(8);
			} while (bin_str.length() >= 8);
		}
	}

	// Output the code for word.
	if (!word.empty()) {
		bin_str += dictionary[word];
	}

	// Make sure the length of the binary string is a multiple of 8
	auto num_bits_extra = bin_str.length() % 8;
	if (num_bits_extra) {
		bin_str += zeros.substr(0, 8 - num_bits_extra);
	}

	// Finish writing to the output
	while (bin_str.length() >= 8) {
		output << static_cast<char>(binaryString2Int(bin_str.substr(0, 8)));
		bin_str = bin_str.substr(8);
	}
}
