/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <string>


std::string int2BinaryString(int c, int cl);
