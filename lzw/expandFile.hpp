/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		February 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <string>


void expandFile(const std::string input_filename,
				const std::string output_filename,
				const std::size_t code_base_len = 12,
				const std::size_t code_max_len = 12,
				const bool reset_full_dictionary = false);
