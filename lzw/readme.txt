High-level Explanation of Implementation
-------------------------------------------------
Part one uses the LZW algorithms to compress and
decompress a passed file using 12 bit encodings.
Part two adds to this by allowing for variable
length encodings.  It starts with 9 bit encodings
and increases length as the dictionary fills up.
Both part one and two stop adding new entries to
the dictionary after the maximum amount of allowed
bits are in use for encoding.  Part two has a
maximum word size of 16 bits.


Notes
-------------------------------------------------
* This code is derived from LZW@RosettaCode for UA CS435
* Part 3 (bonus) lzw435MM is not implemented
* Case3.docx does not work correctly

How to Build:

git clone git@gitlab.com:brandon-staab/algorithms.git &&
mkdir algorithms/lzw/build &&
cd algorithms/lzw/ &&
make


How to Use:

usage: lzw435 <c|e> <input_file>
	c	compresses input_file file and saves it as input_file.lzw
	e	expands input_file.lzw and saves it as input_file

usage: lzw435M <c|e> <input_file>
	c	compresses input_file file and saves it as input_file.lzw2
	e	expands input_file.lzw2 and saves it as input_fileM

usage: lzw435MM <c|e> <input_file>
	c	compresses input_file file and saves it as input_file.lzw3
	e	expands input_file.lzw3 and saves it as input_fileMM
