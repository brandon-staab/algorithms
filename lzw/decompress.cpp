/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "decompress.hpp"

#include "binaryString2Int.hpp"

#include <cmath>
#include <iostream>
#include <map>


std::string
decompress(std::string& bcode, std::size_t word_size, const std::size_t code_max_len, const bool reset_full_dictionary) {
	std::size_t curr_pos = 0, dictSize = 256, curr_max_entries = std::pow(2, word_size);
	std::string entry, result, word;
	std::map<int, std::string> dictionary;

	// Add the ASCII table to the dictionary
	for (auto i = 0; i < dictSize; ++i) {
		dictionary[i] = std::string(1, i);
	}

	// First char
	result = word = binaryString2Int(bcode.substr(curr_pos, word_size));
	curr_pos += word_size;

	// Finish lzw decompression
	while (curr_pos + word_size <= bcode.length()) {
		int k = binaryString2Int(bcode.substr(curr_pos, word_size));
		curr_pos += word_size;

		if (dictionary.count(k)) {  // check if it is in the dictionary
			entry = dictionary[k];
		} else if (k == dictSize) {  // special case
			entry = word + word[0];
		} else {
			throw "Bad compressed k";
		}

		// Add w+entry[0] to the dictionary.
		if (dictionary.size() < curr_max_entries) {
			dictionary[dictSize++] = word + entry[0];
		} else if (word_size >= code_max_len) {  // Check if it surpassed the code_max_len threshold
			if (reset_full_dictionary) {
				throw "Part 3";
			}
		} else {
			curr_pos -= word_size;                        // rollback
			curr_max_entries = std::pow(2, ++word_size);  // recalculate
			continue;                                     // redo
		}

		result += word = entry;
	}

	return result;
}
