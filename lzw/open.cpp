/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "open.hpp"

#include <sys/stat.h>
#include <fstream>
#include <iostream>


namespace {
	const std::string zeros = "00000000";
}


std::string
open(const std::string filename) {
	struct stat filestatus;
	stat(filename.c_str(), &filestatus);
	const std::size_t fsize = filestatus.st_size;  // get the size of the file in bytes

	std::ifstream file(filename.c_str(), std::ios::binary);
	if (!file.is_open()) {
		throw "File was not opened";
	}

	char data[fsize];
	file.read(data, fsize);
	file.close();

	std::string s = "";
	std::size_t count = 0;
	while (count < fsize) {
		unsigned char uc = static_cast<unsigned char>(data[count]);
		std::string bin_str = "";  // a binary string
		for (int j = 0; j < 8 && uc > 0; ++j) {
			bin_str = ((uc % 2 == 0) ? "0" : "1") + bin_str;
			uc >>= 1;
		}

		bin_str = zeros.substr(0, 8 - bin_str.size()) + bin_str;  // pad 0s to left if needed
		s += bin_str;
		count++;
	}

	return s;  // file as string in binary
}
