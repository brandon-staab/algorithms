make fresh &&
echo "Part 1 - Case0.txt" &&
cp ./test_case/case0.txt ./case0.txt &&
./lzw435 c ./case0.txt &&
./lzw435 e ./case0.txt.lzw &&
diff ./test_case/case0.txt ./case0.txt &&
echo "Part 1 - Case1.txt" &&
cp ./test_case/case1.txt ./case1.txt &&
./lzw435 c ./case1.txt &&
./lzw435 e ./case1.txt.lzw &&
diff ./test_case/case1.txt ./case1.txt &&
echo "Part 1 - Bible.txt" &&
cp ./test_case/bible.txt ./bible.txt &&
./lzw435 c ./bible.txt &&
./lzw435 e ./bible.txt.lzw &&
diff ./test_case/bible.txt ./bible.txt &&
echo "Part 1 - Case 3.docx" &&
cp ./test_case/case3.docx ./case3.docx &&
./lzw435 c ./case3.docx &&
./lzw435 e ./case3.docx.lzw &&
diff ./test_case/case3.docx ./case3.docx &&
echo "Part 1 - Monkey.gif" &&
cp ./test_case/monkey.gif ./monkey.gif &&
./lzw435 c ./monkey.gif &&
./lzw435 e ./monkey.gif.lzw &&
diff ./test_case/monkey.gif ./monkey.gif &&
rm -f *.o case* bible* monkey* &&
echo "Part 2 - Case0.txt" &&
cp ./test_case/case0.txt ./case0.txt &&
./lzw435M c ./case0.txt &&
./lzw435M e ./case0.txt.lzw2 &&
diff ./test_case/case0.txt ./case0.txt2M &&
echo "Part 2 - Case1.txt" &&
cp ./test_case/case1.txt ./case1.txt &&
./lzw435M c ./case1.txt &&
./lzw435M e ./case1.txt.lzw2 &&
diff ./test_case/case1.txt ./case1.txt2M &&
echo "Part 2 - Bible.txt" &&
cp ./test_case/bible.txt ./bible.txt &&
./lzw435M c ./bible.txt &&
./lzw435M e ./bible.txt.lzw2 &&
diff ./test_case/bible.txt ./bible.txt2M &&
echo "Part 2 - Case 3.docx" &&
cp ./test_case/case3.docx ./case3.docx &&
./lzw435M c ./case3.docx &&
./lzw435M e ./case3.docx.lzw2 &&
diff ./test_case/case3.docx ./case3.docx2M &&
echo "Part 2 - Monkey.gif" &&
cp ./test_case/monkey.gif ./monkey.gif &&
./lzw435M c ./monkey.gif &&
./lzw435M e ./monkey.gif.lzw2 &&
diff ./test_case/monkey.gif ./monkey.gif2M &&
echo "Cleaning" &&
make clean &&
rm -f *.o case* bible* monkey*
