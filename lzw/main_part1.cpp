/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "compressFile.hpp"
#include "expandFile.hpp"

#include <cstring>
#include <iostream>


int
main(int argc, char const* argv[]) {
	std::string filename;

	if (argc == 3) {
		filename = std::string(argv[2]);
	} else {
		goto usage;
	}

	try {
		if (std::strcmp(argv[1], "c") == 0) {
			compressFile(filename, filename + ".lzw");
		} else if (std::strcmp(argv[1], "e") == 0) {
			expandFile(filename, filename.substr(0, filename.length() - 4));
		} else {
			goto usage;
		}
	} catch (char const* msg) {
		std::cerr << "Exception Caught: " << msg << std::endl;
		return 1;
	}

	std::cout << std::endl;
	return 0;

usage:
	std::cout << "usage: lzw435 <c|e> <input_file>" << std::endl;
	return 1;
}
