/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "int2BinaryString.hpp"

#include <iostream>


std::string
int2BinaryString(int c, int cl) {
	std::string p = "";  // a binary code string with code length = cl
	int code = c;
	while (c > 0) {
		if (c % 2 == 0) {
			p = "0" + p;
		} else {
			p = "1" + p;
		}
		c >>= 1;
	}

	int zeros = cl - p.size();
	if (zeros < 0) {
		std::cout << "\nWarning: Overflow. code " << code << " is too big to be coded by " << cl << " bits!\n";
		p = p.substr(p.size() - cl);
	} else {
		for (int i = 0; i < zeros; i++) {  // pad 0s to left of the binary code if needed
			p = "0" + p;
		}
	}

	return p;
}
