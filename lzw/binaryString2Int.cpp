/**
 *  \brief		Project 2: LZW
 *  \details	Algorithms project for Dr. Duan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		March 2018
 *  \copyright	COPYRIGHT (C) 2018 Brandon Staab (bls114) All rights reserved.
 */

#include "binaryString2Int.hpp"


int
binaryString2Int(std::string p) {
	int code = 0;

	if (p.size() > 0) {
		if (p.at(0) == '1') {
			code = 1;
		}
		p = p.substr(1);

		while (p.size() > 0) {
			code <<= 1;
			if (p.at(0) == '1') {
				code++;
			}
			p = p.substr(1);
		}
	}

	return code;
}
